<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`orders`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $door_width;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $door_height;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $handle_side;

    /**
     * @ORM\OneToMany(targetEntity=Window::class, mappedBy="booking")
     */
    private $windows;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
     */
    private $user_order;

    public function __construct()
    {
        $this->windows = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDoorWidth(): ?string
    {
        return $this->door_width;
    }

    public function setDoorWidth(string $door_width): self
    {
        $this->door_width = $door_width;

        return $this;
    }

    public function getDoorHeight(): ?string
    {
        return $this->door_height;
    }

    public function setDoorHeight(string $door_height): self
    {
        $this->door_height = $door_height;

        return $this;
    }

    public function getHandleSide(): ?string
    {
        return $this->handle_side;
    }

    public function setHandleSide(string $handle_side): self
    {
        $this->handle_side = $handle_side;

        return $this;
    }

    /**
     * @return Collection|Window[]
     */
    public function getWindows(): Collection
    {
        return $this->windows;
    }

    public function addWindow(Window $window): self
    {
        if (!$this->windows->contains($window)) {
            $this->windows[] = $window;
            $window->setBooking($this);
        }

        return $this;
    }

    public function removeWindow(Window $window): self
    {
        if ($this->windows->removeElement($window)) {
            // set the owning side to null (unless already changed)
            if ($window->getBooking() === $this) {
                $window->setBooking(null);
            }
        }

        return $this;
    }

    public function getUserOrder(): ?User
    {
        return $this->user_order;
    }

    public function setUserOrder(?User $user_order): self
    {
        $this->user_order = $user_order;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeImmutable $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }
}
