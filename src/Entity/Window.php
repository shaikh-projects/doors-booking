<?php

namespace App\Entity;

use App\Repository\WindowRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WindowRepository::class)
 * @ORM\Table(name="`windows`")
 */
class Window
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $width;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $height;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="windows")
     */
    private $booking;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWidth(): ?string
    {
        return $this->width;
    }

    public function setWidth(string $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getBooking(): ?Order
    {
        return $this->booking;
    }

    public function setBooking(?Order $booking): self
    {
        $this->booking = $booking;

        return $this;
    }
}
