<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210803141114 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, user_order_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, door_width VARCHAR(255) NOT NULL, door_height VARCHAR(255) NOT NULL, handle_side VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_F52993986D128938 (user_order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993986D128938 FOREIGN KEY (user_order_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE window ADD booking_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE window ADD CONSTRAINT FK_8BE4F9DD3301C60 FOREIGN KEY (booking_id) REFERENCES `order` (id)');
        $this->addSql('CREATE INDEX IDX_8BE4F9DD3301C60 ON window (booking_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE window DROP FOREIGN KEY FK_8BE4F9DD3301C60');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP INDEX IDX_8BE4F9DD3301C60 ON window');
        $this->addSql('ALTER TABLE window DROP booking_id');
    }
}
